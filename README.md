# calk

[![Hackage](https://img.shields.io/hackage/v/calk.svg?logo=haskell)](https://hackage.haskell.org/package/calk)
[![Stackage Lts](http://stackage.org/package/calk/badge/lts)](http://stackage.org/lts/package/calk)
[![Stackage Nightly](http://stackage.org/package/calk/badge/nightly)](http://stackage.org/nightly/package/calk)
[![MIT license](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)

See README for more info
