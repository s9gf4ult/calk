module Main (main) where

import           Calk
import           Calk
import           Control.Monad
import           Data.IORef
import qualified Data.Text.IO  as T

main :: IO ()
main = do
  putStrLn "Calk"
  sVar <- newIORef mempty
  forever $ do
    putStr "> "
    c <- T.getLine
    case parseCmd c of
      Left er -> putStrLn er
      Right cmd -> do
        s <- readIORef sVar
        case exec s cmd of
          Left er -> print er
          Right (v, newS) -> do
            writeIORef sVar newS
            T.putStrLn $ renderValue v
