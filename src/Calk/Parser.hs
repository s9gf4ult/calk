module Calk.Parser where

import           Calk.Eval
import           Control.Applicative
import           Control.Monad
import           Data.Attoparsec.Expr as E
import           Data.Attoparsec.Text
import           Data.Char
import qualified Data.List            as L
import           Data.Ratio
import           Data.Text            (Text)
import qualified Data.Text            as T
import           GHC.Generics         (Generic)

parseExpr :: Text -> Either String Expr
parseExpr t = parseOnly p t
  where
    p = do
      skipSpace
      res <- exprParser
      skipSpace
      endOfInput
      return res

parseCmd :: Text -> Either String Cmd
parseCmd t = parseOnly p t
  where
    p = do
      skipSpace
      res <- commandParser
      skipSpace
      endOfInput
      return res

commandParser :: Parser Cmd
commandParser
  =   assn
  <|> (CExp <$> exprParser)
  where
    assn = do
      v <- var
      skipSpace
      void $ char '='
      skipSpace
      e <- exprParser
      return $ CAssn v e

exprParser :: Parser Expr
exprParser = expr
  where
    braced = do
      void $ char '('
      skipSpace
      expr <- expr
      skipSpace
      void $ char ')'
      return expr
    atomic = varLit <|> braced
    expr = buildExpressionParser ops atomic
      <|> atomic
    cc c = do
      skipSpace
      char c
      skipSpace
    ops =
      [
        [ E.Infix ((BinOp Mul) <$ cc '*') AssocLeft
        , E.Infix ((BinOp Div) <$ cc '/') AssocLeft
        ],
        [ E.Infix ((BinOp Add) <$ cc '+') AssocLeft
        , E.Infix ((BinOp Sub) <$ cc '-') AssocLeft
        ]
      ]

varLit :: Parser Expr
varLit
  =   Var <$> var
  <|> Lit <$> rational

var :: Parser Variable
var = do
  h <- letter
  t <- many $ satisfy isAlphaNum
  return $ Variable $ T.pack $ h : t

plusMinusOp, mulDivOp :: Parser BinOperation
plusMinusOp = do
  c <- satisfy $ (`L.elem` ("+-" :: String))
  case c of
    '+' -> return Add
    '-' -> return Sub
    _   -> fail $ "Unexpected operation: " ++ [c]
mulDivOp = do
  c <- satisfy $ (`L.elem` ("*/" :: String))
  case c of
    '*' -> return Mul
    '/' -> return Div
    _   -> fail $ "Unexpected operation: " ++ [c]
