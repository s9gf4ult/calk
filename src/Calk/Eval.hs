module Calk.Eval where


import           Control.DeepSeq
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import           Data.Monoid
import           Data.Ratio
import           Data.Scientific
import           Data.Text       (Text)
import qualified Data.Text       as T
import           GHC.Generics    (Generic)

type Value = Scientific

data Expr
  = BinOp BinOperation Expr Expr
  -- | Infix InfOp Expr
  | Lit Value
  | Var Variable
  deriving (Eq, Ord, Show, Generic)

instance NFData Expr

data BinOperation = Add | Sub | Mul | Div
  deriving (Eq, Ord, Show, Generic)

instance NFData BinOperation

data InfOp = InfMin
  deriving (Eq, Ord, Show, Generic)

instance NFData InfOp

newtype Variable = Variable
  { unVariable :: Text
  } deriving (Eq, Ord, Show, Generic, NFData)

vname :: Variable -> Text
vname = unVariable

data Cmd
  = CExp Expr
  -- ^ Print out expression
  | CAssn Variable Expr
  -- ^ Assign expression value to variable
  deriving (Eq, Ord, Show, Generic)

newtype S = S
  { unS :: Map Variable Value
  } deriving (Eq, Ord, Show, Generic, Semigroup, Monoid)

setvar :: S -> Variable -> Value -> S
setvar (S m) v e = S $ M.insert v e m

getvar :: S -> Variable -> Maybe Value
getvar (S m) v = M.lookup v m

type Err = Either Error

data Error
  = DivisionByZero
  | VariableNotFound Variable
  deriving (Eq, Ord, Show, Generic)

-- | Executes command on a state and returns it's result with new state
exec :: S -> Cmd -> Err (Value, S)
exec s = \case
  CExp ex -> (,s) <$> eval s ex
  CAssn v ex -> do
    res <- eval s ex
    return (res, setvar s v res)

eval :: S -> Expr -> Err Value
eval s = \case
  BinOp op e1 e2 -> do
    a <- eval s e1
    b <- eval s e2
    binop op a b
  Lit a -> Right a
  Var v -> case getvar s v of
    Nothing -> Left $ VariableNotFound v
    Just a  -> Right a

renderValue :: Value -> Text
renderValue = T.pack . show

binop :: BinOperation -> Value -> Value -> Err Value
binop op a b = case op of
  Add -> pure $ a + b
  Sub -> pure $ a - b
  Mul -> pure $ a * b
  Div -> case b of
    0 -> Left DivisionByZero
    _ -> pure $ a / b
