module TestOrphans where

import           Calk.Eval
import           Data.Monoid
import           Test.SmallCheck
import           Test.SmallCheck.Series

instance (Monad m) => Serial m Cmd

instance (Monad m) => Serial m Expr

instance (Monad m) => Serial m BinOperation

variablesNames :: [Variable]
variablesNames = Variable <$>
  ["a", "b1", "C", "d"]

instance (Monad m) => Serial m Variable where
  series = generate $ \l -> take l variablesNames

instance (Monad m) => Serial m Value where
  series = fromInteger <$> series
