module ParserTest where

import           Calk.Eval
import           Calk.Parser
import           Control.DeepSeq
import           Data.Either
import           Data.Text             (Text)
import qualified Data.Text             as T
import           Test.SmallCheck
import           Test.Tasty.SmallCheck
import           TestOrphans


renderNoBrace :: Expr -> Text
renderNoBrace = \case
  BinOp op a b -> renderExpr a <> renderOp op <> renderExpr b
  Lit l -> renderValue l
  Var v -> renderVar v

renderVar :: Variable -> Text
renderVar (Variable t) = t

renderExpr :: Expr -> Text
renderExpr = \case
  BinOp op a b -> braced (renderExpr a) <> renderOp op <> braced (renderExpr b)
  Lit l -> renderValue l
  Var (Variable t) -> t
  where
    braced :: Text -> Text
    braced t = "(" <> t <> ")"

renderOp :: BinOperation -> Text
renderOp = \case
  Add -> "+"
  Sub -> "-"
  Mul -> "*"
  Div -> "/"

renderCmd :: Cmd -> Text
renderCmd = \case
  CExp e -> renderExpr e
  CAssn v e -> renderVar v <> " = " <> renderExpr e

scprop_parserValid :: Expr -> Bool
scprop_parserValid e =
  let
    t = renderExpr e
    res = parseExpr t
  in Right e == res

scprop_parserNotFails :: Expr -> Bool
scprop_parserNotFails e =
  let
    t = renderNoBrace e
    res = case parseExpr t of
      Left  _ -> False
      Right a -> mulOnlyInMul a
  in res

-- | Check that parser returned correct nesting. (It may not match with initial
-- expr)
mulOnlyInMul :: Expr -> Bool
mulOnlyInMul = \case
  Lit x -> deepseq x True
  Var v -> deepseq v True
  BinOp op a b -> if addSub op
    then mulOnlyInMul a && mulOnlyInMul b
    else onlyMul a && onlyMul b
  where
    onlyMul = \case
      Lit x -> deepseq x True
      Var v -> deepseq v True
      BinOp op a b -> if not $ addSub op
        then onlyMul a && onlyMul b
        else False
    addSub = \case
      Add -> True
      Sub -> True
      _ -> False

scprop_cmdParserWorks :: Cmd -> Bool
scprop_cmdParserWorks c =
  let
    t = renderCmd c
    res = parseCmd t
  in Right c == res
