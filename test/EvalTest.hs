module EvalTest where


import           Calk.Eval
import           Calk.Parser
import           Control.DeepSeq
import           Data.Either
import qualified Data.Map.Strict       as M
import           Data.Text             (Text)
import qualified Data.Text             as T
import           Test.HUnit
import           Test.SmallCheck
-- import           Test.Tasty.HUnit
import           Test.Tasty.SmallCheck
import           TestOrphans

scprop_EvalWorks :: Expr -> Bool
scprop_EvalWorks e =
  let
    s = S $ M.fromList $ zip variablesNames $ repeat 1
    res = case eval s e of
      Left DivisionByZero -> True -- Expected error for some exprs
      Right res           -> deepseq res True
      _                   -> False
  in res

unit_123 :: Assertion
unit_123 = do
  let
    Right expr = parseExpr "1 - 2 - 3"
    Right r = eval mempty expr
  assertEqual "Fail" r (-4)
